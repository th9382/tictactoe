// TODO styling - diff colors for players
// TODO add different names

function Player(name, symbol_type) {
    this.name = name;
    this.symbol_type = symbol_type;
}

function Tile(row, column) {
  this.content = " ";
  this.row = row;
  this.column = column;
  this.htmlClass = "cell";
}

Tile.prototype.setContentValue = function(tile_content) {
  this.content = tile_content;
};

function Grid() {
  this.grid = [];
  this.x = 3; // dimensions
  this.y = 3;
}
Grid.prototype.buildGrid = function() {
  for(var i = 0; i < this.x; i++) {
    arr_temp = [];
    for(var j = 0; j < this.y; j++) {
      tile = new Tile(i, j);
      arr_temp.push(tile);
    }
    this.grid.push(arr_temp);
  }
};
Grid.prototype.displayGrid = function(tag) {
  tag.children().remove();
  for(var i = 0; i < this.grid.length; i++) {
    for(var j = 0; j < this.grid[i].length; j++) {
      tile = this.grid[i][j];
      tag.append('<div id="c_'+tile.row+'_'+tile.column+'" class="'+tile.htmlClass+'"><span>'+tile.content+'</span></div>');
    }
  }
};

function Game(player1, player2, grid) {
  this.currentPlayer = player1;
  this.player1 = player1;
  this.player2 = player2;
  this.grid = grid;

  this.setup = function() {
    this.grid.buildGrid();
    this.grid.displayGrid($(".wrapper"));
  };
}

Game.prototype.switchPlayer = function() {
  this.currentPlayer = this.currentPlayer == this.player1 ? this.player2 : this.player1;
};

Grid.prototype.updateTile = function(coordinates, value) {
  var current_tile = this.grid[coordinates[0]][coordinates[1]];
  if( current_tile.content === " " ) {
    current_tile.setContentValue(value);
    if( value === "O" ) {
      current_tile.htmlClass += " p1";
    } else {
      current_tile.htmlClass += " p2";
    }
    return true;
  } else {
    return false;
  }
};

// FIXME - simplify
Game.prototype.isWinner = function () {
  var arr = this.grid.grid;
  if( arr[0][0].content === arr[0][1].content && arr[0][0].content === arr[0][2].content && arr[0][0].content !== " " ) {
    return true;
  } else if( arr[1][0].content === arr[1][1].content && arr[1][0].content === arr[1][2].content && arr[1][0].content !== " ") {
    return true;
  } else if( arr[2][0].content === arr[2][1].content && arr[2][0].content === arr[2][2].content && arr[2][0].content !== " " ) {
    return true;
  } else if (arr[0][0].content === arr[1][0].content && arr[0][0].content === arr[2][0].content && arr[0][0].content !== " " ) {
    return true;
  } else if (arr[0][1].content === arr[1][1].content && arr[0][1].content === arr[2][1].content && arr[0][1].content !== " " ) {
    return true;
  } else if (arr[0][2].content === arr[1][2].content && arr[0][2].content === arr[2][2].content && arr[0][2].content !== " " ) {
    return true;
  } else if (arr[0][0].content === arr[1][1].content && arr[0][0].content === arr[2][2].content && arr[0][0].content !== " " ) {
    return true;
  } else if (arr[0][2].content === arr[1][1].content && arr[0][2].content === arr[2][0].content && arr[0][2].content !== " " ) {
    return true;
  } else {
    return false;
  }
};

Game.prototype.isDraw = function() {
  var arr = this.grid.grid;
  for(var i = 0; i < arr.length; i++) {
    for(var j = 0; j < arr[i].length; j++) {
      if(arr[i][j].content === " ") {
        return false;
      }
    }
  }
  return true;
};

Game.prototype.play = function() {
  // TODO cleanup
  $("div").on('click', ".cell", function(e) {
    e.stopPropagation();
    e.preventDefault();
    var tileId = $(this).attr("id");
    var coordinates = getLocation(tileId);
    var updatedTile = game.grid.updateTile(coordinates, game.currentPlayer.symbol_type);
    game.grid.displayGrid($(".wrapper"));
    if (!game.isDraw() && !game.isWinner() && updatedTile) {
      game.switchPlayer();
      $('#current_player').text(game.currentPlayer.name + "'s turn");
    } else if (game.isWinner()) {
      $('#current_player').text(game.currentPlayer.name + ' wins!');
      $("div").off('click');
      $('.wrapper').addClass('animated shake');
    } else if (game.isDraw()){
      $('#current_player').text("It's a draw!");
      $("div").off('click');
    }
  });
};

var getLocation = function getLocation(string) {
  arr = string.split('_');
  var row = parseInt(arr[1]);
  var column = parseInt(arr[2]);
  return [row, column];
};


var newGame = function() {
  var p1 = new Player('Player 1', 'O');
  var p2 = new Player('Player 2', 'X');
  var grid = new Grid();
  game = new Game(p1, p2, grid);
  game.setup();
  $('#current_player').text(game.currentPlayer.name + "'s turn");
  $(".wrapper").removeClass('animated shake');
  game.play();
};


$(document).ready(function() {
  newGame();

  $(document).keydown(function(e) {
    var key_code = e.which;
      if ( key_code === 82 ) { // 'r' for resetting game
        $("div").off('click');
        newGame();
    }
  });

  $("button").on('click', function() {
    $("div").off('click');
    newGame();
  });
});
